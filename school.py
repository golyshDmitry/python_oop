import random


class Data:
    def __init__(self, *info):
        self.info = list(info)

    def __getitem__(self, i):
        return self.info[i]


class Teacher:
    @staticmethod
    def teach(info, *pupil):
        for i in pupil:
            i.take(info)


class Pupil:
    def __init__(self):
        self.knowledge = []

    def take(self, info):
        self.knowledge.append(info)

    def forget(self):
        del self.knowledge[random.randint(0, len(self.knowledge) - 1)]


def main():
    lesson = Data('class', 'object', 'inheritance', 'polymorphism', 'encapsulation')
    t = Teacher()
    p1 = Pupil()
    p2 = Pupil()
    t.teach(lesson[2], p1, p2)
    t.teach(lesson[0], p1)
    print(*p1.knowledge)

    p2.take(lesson[-1])

    print(*p2.knowledge)

    p1.forget()

    print(*p1.knowledge)


if __name__ == '__main__':
    main()
