class Human:
    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return "name: " + str(self.name) + " age: " + str(self.age)

    def __add__(self, other):
        return Human(str(self.name) + '_' + str(other.name),
                     self.age + other.age)

    __radd__ = __add__


def main():
    h1 = Human("Alex", 24)
    h2 = Human("Huston", 28)

    print(h1 + h2)


if __name__ == '__main__':
    main()
