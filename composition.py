import math


class Room:
    def __init__(self, w, h, l):
        self.width = w
        self.height = h
        self.length = l
        self.squares = list()

    def add_square(self, w, h):
        self.squares.append(Squares(w, h))

    def work_surface(self):
        surface = 2 * self.height * (self.width + self.length)
        for i in self.squares:
            surface -= i.square
        return surface

    def rolls_of_wallpaper(self, w, h):
        return math.ceil(self.work_surface() / (w * h))


class Squares:
    def __init__(self, w, h):
        self.square = w * h


def main():
    room = Room(2, 3, 4)

    room.add_square(1, 2)
    room.add_square(1, 1)

    print(room.work_surface())

    print(room.rolls_of_wallpaper(1, 5))


if __name__ == '__main__':
    main()
