class Anime:
    def __init__(self, title, series):
        self.__title = title
        self.__series = series
        self.__progress = '0/{}'.format(self.__series)
        self.__rating = '0/10'

    def __str__(self):
        return '\ntitle: ' + str(self.__title) + '\nseries: ' + \
               str(self.__series) + '\nprogress: ' + str(self.__progress) + \
               '\nrating: ' + str(self.__rating)

    def set_progress(self, n):
        self.__progress = '{}/{}'.format(n, self.__series)

    def set_rating(self, n):
        self.__rating = '{}/10'.format(n)


def main():
    a = Anime('Naruto', 720)
    print(a)

    a.set_progress(25)
    a.set_rating(10)

    print(a)


if __name__ == '__main__':
    main()
