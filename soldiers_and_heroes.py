from Units import *
import random


def main():
    team_red = list()
    team_blue = list()

    team_red.append(Hero(1, "red", 10))
    team_blue.append(Hero(2, "blue", 10))

    for i in range(10):
        temp = random.randint(0, 1)
        if temp == 0:
            team_red.append(Soldier(i, "red"))
        else:
            team_blue.append(Soldier(i, "blue"))

    print(*team_red)
    print(*team_blue)

    if len(team_red) > len(team_blue):
        team_red[0].level_up()
        print("Hero", team_red[0].number, "leveled up!\n")
    elif len(team_blue) > len(team_red):
        team_blue[0].level_up()
        print("Hero", team_blue[0].number, "leveled up!\n")

    print(*team_red)
    print(*team_blue)

    try:
        random.choice(team_red[1:]).follow_the_hero(team_red[0])
    except IndexError:
        print("Team Red has no soldiers!")


if __name__ == '__main__':
    main()
