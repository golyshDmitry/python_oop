class Warrior:
    def __init__(self, n, h, d):
        self.name = n
        self.health = h
        self.damage = d

    def __str__(self):
        return "Warrior " + self.name + ".\nHealth: " \
               + str(self.health) + ".\nDamage: " + str(self.damage) + "."
