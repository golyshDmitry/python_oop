class Unit:
    def __init__(self, number, team):
        self.number = number
        self.team = team

    def __str__(self):
        return 'number ' + str(self.number) + ' team ' + str(self.team)


class Soldier(Unit):
    def follow_the_hero(self, hero):
        print("Soldier number", self.number,
              "follows the hero number", hero.number)

    def __str__(self):
        return 'Soldier ' + Unit.__str__(self) + '\n'


class Hero(Unit):
    def __init__(self, number, team, level):
        Unit.__init__(self, number, team)
        self.level = level

    def __str__(self):
        return 'Hero ' + Unit.__str__(self) + ' level ' + str(self.level) + '\n'

    def level_up(self):
        self.level += 1
