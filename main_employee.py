from Employee import Employee


def main():
    emp1 = Employee("Alex", "Armstrong", 3)
    emp2 = Employee("Roy", "Mustang", 5)
    emp3 = Employee("Uxi", "From-Hill")

    print(emp1, '\n')
    print(emp2, '\n')
    print(emp3, '\n')

    del emp3

    input()


if __name__ == '__main__':
    main()
