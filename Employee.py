class Employee:
    def __init__(self, name, surname, qualification=1):
        self.name = name
        self.surname = surname
        self.qualification = qualification

    def __str__(self):
        return self.name + ' ' + self.surname + ' ' + str(self.qualification)

    def __del__(self):
        print("Goodbye, mr.", self.name, self.surname)
