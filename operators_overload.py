import math


class Snow:
    def __init__(self, n):
        self.snowflakes = n

    def __call__(self, n):
        self.snowflakes = n

    def __add__(self, n):
        self.snowflakes += n

    def __sub__(self, n):
        self.snowflakes -= n

    def __mul__(self, n):
        self.snowflakes *= n

    def __truediv__(self, n):
        self.snowflakes = math.ceil(self.snowflakes / n)

    __radd__ = __add__
    __rsub__ = __sub__
    __rmul__ = __mul__
    __rtruediv__ = __truediv__

    def make_snow(self, n):
        print(('*' * n + '\n') * (self.snowflakes // n) + \
               ('*' * (self.snowflakes % n)))
        print()


def main():
    snow = Snow(20)

    snow.make_snow(5)

    2 + snow

    snow.make_snow(5)

    snow(16)

    snow.make_snow(4)


if __name__ == '__main__':
    main()
