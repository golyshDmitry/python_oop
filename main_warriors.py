from Warrior import Warrior
import random


def main():
    print("name    health    damage")
    w1 = Warrior(input(), int(input()), int(input()))
    w2 = Warrior(input(), int(input()), int(input()))

    print(w1)
    print(w2)

    print()

    while True:
        temp = random.choice([w1, w2])
        if temp.name == w1.name:
            print(w1.name, "attacks.")
            w2.health -= w1.damage
            print(w2.name, "s health: ", w2.health, sep='')
            if w2.health <= 0:
                print(temp.name, "wins!")
                break
        else:
            print(w2.name, "attacks.")
            w1.health -= w2.damage
            print(w1.name, "s health: ", w1.health, sep='')
            if w1.health <= 0:
                print(temp.name, "wins!")
                break


if __name__ == '__main__':
    main()
