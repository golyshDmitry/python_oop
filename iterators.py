def decorator(foo):

    def paper_foo():
        print('Bellow')
        foo()
        print('Under')

    return paper_foo


def also_decorator(foo):

    def also_paper_foo():
        print('up')
        foo()
        print('down')

    return also_paper_foo


@also_decorator
@decorator
def alone_foo():
    print('My name is alone_foo.')


alone_foo()
